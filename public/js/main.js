"use strict";
(() => {
    const recodeTable = document.body.querySelector('#table-win-lose_recode');
    if (!recodeTable)
        throw new Error('勝敗表示の表が見つからない');
    const firstCountOutput = recodeTable.querySelector('#td-first-count');
    if (!firstCountOutput)
        throw new Error('先攻の回数の表示要素が見つからない');
    const firstWinCountOutput = recodeTable.querySelector('#td-first-win-count');
    if (!firstWinCountOutput)
        throw new Error('先攻の勝数の表示要素が見つからない');
    const firstLoseCountOutput = recodeTable.querySelector('#td-first-lose-count');
    if (!firstLoseCountOutput)
        throw new Error('先攻の敗数の表示要素が見つからない');
    const firstWinrateOutput = recodeTable.querySelector('#td-first-winrate');
    if (!firstWinrateOutput)
        throw new Error('先攻の勝率の表示要素が見つからない');
    const secondTotalCountOutput = recodeTable.querySelector('#td-second-count');
    if (!secondTotalCountOutput)
        throw new Error('後攻の回数の表示要素が見つからない');
    const secondWinCountOutput = recodeTable.querySelector('#td-second-win-count');
    if (!secondWinCountOutput)
        throw new Error('後攻の勝数の表示要素が見つからない');
    const secondLoseCountOutput = recodeTable.querySelector('#td-second-lose-count');
    if (!secondLoseCountOutput)
        throw new Error('後攻の敗数の表示要素が見つからない');
    const secondWinrateOutput = recodeTable.querySelector('#td-second-winrate');
    if (!secondWinrateOutput)
        throw new Error('後攻の勝率の表示要素が見つからない');
    const totalCountOutput = recodeTable.querySelector('#td-total-count');
    if (!totalCountOutput)
        throw new Error('全体の回数の表示要素が見つからない');
    const totalWinCountOutput = recodeTable.querySelector('#td-total-win-count');
    if (!totalWinCountOutput)
        throw new Error('全体の勝数の表示要素が見つからない');
    const totalLoseCountOutput = recodeTable.querySelector('#td-total-lose-count');
    if (!totalLoseCountOutput)
        throw new Error('全体の敗数の表示要素が見つからない');
    const totalWinrateOutput = recodeTable.querySelector('#td-total-winrate');
    if (!totalWinrateOutput)
        throw new Error('全体の勝率の表示要素が見つからない');
    const firstWinInput = document.body.querySelector('#input-first-win');
    if (!firstWinInput)
        throw new Error('先攻の勝数の入力欄が見つからない');
    const firstLoseInput = document.body.querySelector('#input-first-lose');
    if (!firstLoseInput)
        throw new Error('先攻の敗数の入力欄が見つからない');
    const secondWinInput = document.body.querySelector('#input-second-win');
    if (!secondWinInput)
        throw new Error('後攻の勝数の入力欄が見つからない');
    const secondLoseInput = document.body.querySelector('#input-second-lose');
    if (!secondLoseInput)
        throw new Error('後攻の敗数の入力欄が見つからない');
    const calculateWinrate = (winCount, totalCount) => {
        if (!totalCount)
            return "0%";
        return `${(winCount / totalCount * 100).toPrecision(3)}%`;
    };
    const updateTable = () => {
        const firstWinCount = Number(firstWinInput.value);
        firstWinCountOutput.textContent = firstWinCount.toString();
        const firstLoseCount = Number(firstLoseInput.value);
        firstLoseCountOutput.textContent = firstLoseCount.toString();
        const firstCount = firstWinCount + firstLoseCount;
        firstCountOutput.textContent = firstCount.toString();
        firstWinrateOutput.textContent = calculateWinrate(firstWinCount, firstCount);
        const secondWinCount = Number(secondWinInput.value);
        secondWinCountOutput.textContent = secondWinCount.toString();
        const secondLoseCount = Number(secondLoseInput.value);
        secondLoseCountOutput.textContent = secondLoseCount.toString();
        const secondCount = secondWinCount + secondLoseCount;
        secondTotalCountOutput.textContent = secondCount.toString();
        secondWinrateOutput.textContent = calculateWinrate(secondWinCount, secondCount);
        const totalWinCount = firstWinCount + secondWinCount;
        totalWinCountOutput.textContent = totalWinCount.toString();
        const totalLoseCount = firstLoseCount + secondLoseCount;
        totalLoseCountOutput.textContent = totalLoseCount.toString();
        const totalCount = firstCount + secondCount;
        totalCountOutput.textContent = totalCount.toString();
        totalWinrateOutput.textContent = calculateWinrate(totalWinCount, totalCount);
    };
    const inputs = [firstWinInput, firstLoseInput, secondWinInput, secondLoseInput];
    inputs.forEach(input => { input.addEventListener('change', updateTable); });
})();
